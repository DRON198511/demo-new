<?php
/*Post type local information */
function local_info() {
   $labels = array(
        'name'               => 'Local info',
        'singular_name'      => 'Local info',
        'add_new'            => 'Add local info',
        'add_new_item'       => 'Add New local info',
        'edit_item'          => 'Edit local info',
        'new_item'           => 'New local info',
        'all_items'          => 'All local info',
        'view_item'          => 'View local info',
        'search_items'       => 'Search local info',
        'not_found'          => 'No local info found',
        'not_found_in_trash' => 'No local info found in Trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Local info'
   );
   $args = array(
        'labels'             => $labels,
        'public'             => false,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => false,
        'rewrite'            => array( 'slug' => 'local-info' ),
        'capability_type'    => 'post', 
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-warning',
        'supports'           => array( 'title' )
   );
   register_post_type( 'local_info', $args );
}
add_action( 'init', 'local_info' );
