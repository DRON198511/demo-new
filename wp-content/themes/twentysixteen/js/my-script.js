(function ($, window, document) {
  'use strict';

  var page = {
    init: function () {
      page.btn_click();
    },

    btn_click: function () {
      $('form#btn_test').on('click', '#sbmt', function(e){
        e.preventDefault();

        if ($(this).hasClass('red')) {

          var date = new Date();
          var hours = date.getHours();
          var minutes = (date.getMinutes()<10?'0':'') + date.getMinutes();
          var seconds = (date.getSeconds()<10?'0':'') + date.getSeconds();

          var localtime = hours + ':' + minutes + ':' + seconds;

          $.ajax({
            url: ajaxvars.ajaxurl,
            data: {
              'action': 'test_click_ajax',
              'localtime': localtime,
              'localip' : $('form#btn_test #local-ip').val(),
              'post_id': $('form#btn_test #post-id').val()
            },
            cache: false,
            type: 'POST',
            success: function success(response) {
              if (response) {
                $('.site-content').find('.result').html(response);
                $('form#btn_test').find('#sbmt').removeClass('red');
                $('form#btn_test').find('#sbmt').addClass('green');
                $('form#btn_test').find('#sbmt').val('ОТЛИЧНО');
              } else {
                $('.site-content').find('.result').text('Sorry, but there was a data error. Please try again later!');
              }

            }
          });

        }
      });

    },

    load: function () {
    },
    resize: function () {
    },
    scroll: function () {
    }
  };

  $(document).ready(page.init);
  $(window).on({
    'load': page.load,
    'resize': page.resize,
    'scroll': page.scroll
  });
})(jQuery, window, document);