<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
 //Added by WP-Cache Manager

define('DB_NAME', 'demo-new');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z]gD,#d+ox6tar-gt#3xbO6|.ozV1#K+p+xh1D;vMCUi@@yp|(Dc5@1t-T68Uk?e');
define('SECURE_AUTH_KEY',  '>lAVw {/f? CVC%bW,@J|T*]/WPTJD@nPQH>z|yvu`#8*:VG&gA$^#<>7ph:Y@@]');
define('LOGGED_IN_KEY',    'j64{cuEH-]~NGb906W_EMwas-w!tHk:x]x}6+sn_Xy|}TI1|} XG>#6+iKid0M+/');
define('NONCE_KEY',        'deBGz!OHD:2<WT)cQ~+4[%~QQN>yqAkH#g,Ag#j{j7Hdw]-V(u7Ir6$+y/!^B{jf');
define('AUTH_SALT',        'K.+-A[*X%D79=^0)WeayK1i3&C:k>&PWbbM<+E.VkrRqp%ANV5qdV,9cf/XZ|/4d');
define('SECURE_AUTH_SALT', 'hgWwU49>_E~fZN&3%Ao`AW ,!JY=3hpgdhnE[Ex|M:IVAC2`dcj;7&Ff<-0;D$Jp');
define('LOGGED_IN_SALT',   'ESBZ/`-++~B&=/#V&uFAW@/RObR$W0&~[g%-QznhFdWfR{9!e+7Tn18x9hDj5WKN');
define('NONCE_SALT',       '  g5&;FO0y3am=Nn^_X0ddOcpao9|UA-g:1Yu`8]rQqDEhU+:Lw#/d-gF&5#z5R6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
